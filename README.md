<h1>Name: Task</h1>
<h2>Requirements: </h2>
<h3>Commit here your "ready to install" Yeoman generator WebApp.</h3>
<br />
<h2>How to:</h2>
<ol>
  <li>Register User Account at https://gitlab.com</li>
  <li>Send me your UserName to I share with you access, or just request access</li>
  <li>Clone repository at your computer with Git</li>
  <li>Create folder with your UserName</li>
  <li>Create branch with your UserName</li>
  <li>move to your branch</li>
  <li>Install Yeoman generator WebApp at folder name as your UserName you've been created</li>
  <li>Commit it in your branch you've been created with some comments(folders bower-components and node-modules should not be commited)</li>
  <li>Push changes on live</li>
  <li>Create merge request to merge YourBranch with master</li>
</ol>
<h2>Links need to use:</h2>
<ul>
  <li><a href="http://yeoman.io/">http://yeoman.io/</a></li>
  <li><a href="https://githowto.com/">https://githowto.com/</a></li>
  <li><a href="https://gitlab.com">https://gitlab.com</a></li>
  <li><a href="https://docs.google.com/a/w3.co/document/d/1y7dJFJpMlIqrKnFuM0jZDoSTZe7hm62cq4K9u8C8YD0/edit?usp=sharing">https://docs.google.com/a/w3.co/document/d/1y7dJFJpMlIqrKnFuM0jZDoSTZe7hm62cq4K9u8C8YD0/edit?usp=sharing</a> <strong>git usual commands</strong></li>
</ul>
<h2>Your reward</h2>
<ol>
  <li>Learn how to use Git</li>
  <li>Learn new scallfolding tool Yeoman</li>
  <li>Learn how to create gulp project</li>
</ol>

